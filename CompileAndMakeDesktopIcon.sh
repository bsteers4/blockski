#!/usr/bin/env bash

GUI="gb.gui"
Message() { if [ ! -z "$3" ]; then BUTS=", \"$3\""; fi; if [ ! -z "$4" ]; then BUTS="$BUTS, \"$4\""; fi; if [ ! -z "$5" ]; then BUTS="$BUTS, \"$5\""; fi; Message=$(echo -e 'use "'$GUI'"\nPrint Message.'$1'("'$2'"'$BUTS')'| gbs3); }

# Get application directory and exe name
WD=${0%/*}
AppName=${WD##*/}
if [ -e "./.settings" ]; then
  eval $(cat ./.settings|grep Path=)
fi
if [ -z "$Path" ]; then eval $(cat ./.project|grep Path=); fi
if [ -z "$Path" ]; then Path="$AppName.gambas"; fi

# get desktop path

DESK=$(xdg-user-dir DESKTOP)

Message "Question" "$DESK"

# ask what launcher icons to make

REP=$(zenity --list --height=200 --width=300 --checklist --text="Compile $AppName in\n$WD\n\nSelect launcher icons to create after compiling" --column=Enable --column="Make launcher type" "" "Make menu" "" "Make desktop icon")
ERR=$?

  if [ $ERR -eq 1 ]; then 
  echo "User canceled"
  exit; 
  fi

DODTOP=0; DOMENU=0

if [ ! -z "$REP" ]; then
 if [  "${REP%|*}" == "Make menu" ]; then DOMENU=1; fi
 if [  "${REP#*|}" == "Make desktop icon" ]; then DODTOP=1; fi
fi

 if [ $[$DOMENU+$DODTOP] -eq 0 ]; then ERR=1; fi

cd "$WD"

# find what flags to pass gbc3 by reading the .project info
ControlPublic=0
ModulePublic=0
eval $(cat .project|grep Public)

KeepDebugInfo=1
eval $(cat .project|grep KeepDebugInfo)

echo "Compiling gambas executable..."
FLG="-wax"
MODE="-f"
CHK=$(gbc3 --help| grep public-module| awk {'print $1'})
if [[ $CHK != "-f" ]]; then
MODE="-"
fi
if [ $KeepDebugInfo -eq 1 ]; then FLG="$FLG"g; fi
if [ $ControlPublic -eq 1 ]; then FLG="$FLG "$MODE"public-control"; fi
if [ $ModulePublic -eq 1 ]; then FLG="$FLG "$MODE"public-module"; fi

# compile the application
gbc3 $FLG
gba3

if [ $ERR -eq 1 ]; then exit; fi 

echo "Making launcher..."
# select launcher icon alternative actions.
OPTS=$(zenity --forms --text="Select Alternative Actions for the icon.(options with right click)" --add-combo="GUI Toolkits (select gtk/qt5/etc)" --combo-values="|Add All|Add Non-default" --add-combo="Gambas Project Edit (open in gambas IDE)" --combo-values="|Add Project Edit")

if [ $? -eq 1 ]; then zenity --info --width=400 --text="Create icon canceled."; exit; fi

if [ -z "$OPTS" ]; then OPTS=" | "; fi
GUIMODE="${OPTS%|*}"
EDTMODE="${OPTS#*|}"

if [[ $GUIMODE != " " ]]; then
echo -e "Use \"gb.gui\"\nPublic Sub Main()\nIf Args.Count = 1 Then\nPrint Env[\"GB_GUI\"]\nQuit\nEndif\nIf Args[1] = Env[\"GB_GUI\"] Then Quit 0\nQuit 1\nEnd\n" >/tmp/gui
GUIS=""
DEF=$(gbs3 /tmp/gui)
ACTS=""

for s in gtk gtk3 qt4 qt5;do 
if [ ! -e "/usr/lib/gambas3/gb.$s.component" ]; then continue; fi
if [[ "$GUIMODE" != "Add All" ]]; then
  if [[ "gb.$s" == "$DEF" ]]; then continue; fi; 
fi
GB_GUI=gb.$s gbs3 /tmp/gui gb.$s 2>/dev/null;
if [ $? -eq 0 ]; then 
SS=${s^^}
if [ "$SS" == "GTK" ]; then SS="GTK2"; fi
GUIS=$GUIS"RUN_$SS;"
ACTS="$ACTS\n[Desktop Action RUN_$SS]\nName=Run with $SS\nExec=env GB_GUI=gb.$s '$WD/$Path'\n"
fi
done
fi

if [[ $EDTMODE != " " ]]; then
GUIS=$GUIS"EDITG;"
ACTS="$ACTS\n[Desktop Action EDITG]\nName=Edit with Gambas3\nExec=gambas3 '$WD'\n"
fi

if [ -z "$ACTS" ]; then
  echo "no actions to add"
else
  ACTS="Actions=${GUIS%*;}\n$ACTS"
  echo "Adding Actions=${GUIS%*;}"
fi

# try to get icon from .project file or use default
IC=$(cat "$WD/.project" |grep Icon=)

if [ -z "$IC" ]; then Icon=".icon.png"; else Icon=${IC##*=}; fi

if [ $DOMENU -eq 1 ]; then CATS="Categories=Game;\n"; fi

TEXT="[Desktop Entry]\nType=Application
Name=$AppName
Icon=$WD/$Icon
Exec=$WD/$Path
$CATS$ACTS"

MSG="All Done"
if [ $DODTOP -eq 1 ]; then
echo -e "$TEXT" >"$DESK/$AppName.desktop"
chmod +x "$DESK/$AppName.desktop"
MSG="$MSG\n$XDG_DESKTOP_DIR/$AppName.desktop created."
fi
if [ $DOMENU -eq 1 ]; then
echo -e "$TEXT" >"$HOME/.local/share/applications/$AppName.desktop"
MSG="$MSG\n$RES menu created."
fi

zenity --info --width=400 --text="$MSG"

