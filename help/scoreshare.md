
# The ScoreShare server.

When you load the ScoreShare server you can share your scores with others or you can merge other peoples scores into your own.

This feature was essentially made so my girlfriend and I could play the game on our own laptops but share the same scoreboard.

### How to use...

#### Submitting your scores.
* When opening the ScoreShare server window you see a list of all the available user scoreboards.
* Press the "Upload" button to submit your scores to the list.

#### Merging other peoples scores into your own.
* Select a users name to show their scores.
* Scores for a single board can be merged or merge all the boards at once.
* Sharing is read only. meaning you can only merge other scores into your own. to share scores with another user you must both merge each others scores.

#### Sharing your custom made External boards/scores.
* In the board editor there is an option to "Share on ScoreServer", select this to include the board in your list when you share.

Sharing of boards is not yet implemented only scores.

I plan to allow custom boards to be imported as well as their scores via the ScoreShare server.

For now you will just have to transfer custom board files between yourselves manually.
