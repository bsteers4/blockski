<center>

### Blockski , A Klotski like game
#### Written by Bruce Steers
Current status: Work in progress, Beta

The game play board is stable but there are some bugs in the editor undo system.</p>
</center>

### Gameplay:
* Move blocks around the board in order to get the Prize block (the one with the star) to the Goal block (the grey one with a star) as quickly and in as few moves as possible.

* Blocks can only be moved into free spaces and if the path is not blocked by other blocks, walls or ghost walls.

* A block moved any distance and dropped counts as 1 move.

* Only the Prize block and "Ghosted" blocks can pass through ghost walls and the goal block.

* Vanishing walls disappear if the Prize block moves over on them.

* 'Ghosted' blocks cannot go through normal blocks/walls.

#### Program features...

* Adjust block size to make map fit screen/window.
* Online updater, Lets you know if there is a new version and if so you can either download a zip or auto-upgrade.
* Save/Restore game, Either use Quicksave (no filename) or give a name to save / restore game progress. Saves and restores the block positions and game duration.

* Manage Highscores.  Highscores can be saved and imported/merged into other games on other computers.<br>
Use to share your scoreboard with a friend/partner.<br>
Recover Highscores lost if you changed a board filename. shows a list of all recorded scores that you can import.

* ScoreShare. connect to an online score sharing system. once connected your scores stay on the server for 24 hrs and anyone can merge your scoreboard into theirs.
* Games can be paused/resumed
* Added a little side game that loads any chosen picture into the board as a sliding square puzzle. (wip)

#### Board editor features...
* Create/Delete/Edit boards.
* Create/Delete blocks.
* Edit block shapes.
* Use custom board background pictures.
* Use custom block/wall images.
* Undo/Redo.
* Use a color mask on block/wall images.
* Cut/Copy/Paste.
* Colorize or change images for all blocks of the selected type at once.
* Enable/Disable editing in order to test board.
* Reselet option reselects the add-block button so many blocks can be added by just clicking the board free space.

#### Notes:

Built in boards can be added/edited/deleted using the board editor if you open the project in the gambas3 IDE and run it from there.\
Saving/deleting built in boards is disabled when running the executable nomally (not via the IDE), they can be edited but you must then use "Save As" to save to the external boards directory or somewhere else.\
You can oveide this using the command line argument -

Feel free to make new boards and send them to me :)

Warning: Edits to existing Built in boards will be automatically overwritten if updated.

----
A custom block image can be any size but it should be square.
----
Online updater will work differently depending on if the program resides in it's project folder or not.

If it IS in the project folder then the latest project files are downloaded and the whole project source is updated.<br>
(existing files overwrite old ones but nothing else is deleted in case you have made your own boards in the project dir)

If it is a standalone application (just the Blockski+.gambas executable not in the project folder) then the new version is downloaded and compiled in a temporary location and the old Blockski+.gambas file is overwritten.

----
For more help read the tooltips that popup for each control.

#### Todo:
* Make more boards
* Debug the Editor some more.

#### Snapshots:

The main game window...
<img src="http://bws.org.uk/img/BlockskiMain.png">


Board editor...

<img src="http://bws.org.uk/img/BlockskiEditor.png">

#### Attributions / Thanks..
Benoit Minisini for the gambas basic development environment

Textures: Image by <a href="https://www.freeimages.com">Freeimages.com</a>
Wood Photo by <a href="/photographer/tome213-49933">tome213</a> on <a href="/">Freeimages.com</a>
